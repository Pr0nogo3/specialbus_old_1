# Cosmonarchy melee AI
- This document details features of the melee AI for Cosmonarchy.

### Useful links:
- Project info - http://www.fraudsclub.com/cosmonarchy/
- Project downloads - https://gitlab.com/the-no-frauds-club/release/Cosmonarchy
- Discord server - https://discordapp.com/invite/s5SKBmY

### Initial UMS setup:
- In the location layer, create a location that covers the AI's start location
- Using the trigger action 'Run AI script at location',
  - Run <race> expansion custom level in a location placed over the starting town
  - Set starting resources as desired (AI assumes 100 minerals and 0 gas)
  - For optional AI support, leave the controller type as 'human' (in multiplayer mode, Cosmonarchy converts empty human slots to computers)

### AI configuration:
- For best results, the following deaths should be set at or before script runtime!
- Set disruption field deaths for core personalities and dark swarm deaths for early-game build orders (see BUILD ORDERS)
- To establish preferences for army compositions in the early-game, set deaths of specific units to above 0 (see PREFERENCES)
- To enable debug messages for showing AI personalities and build orders, set start location deaths above 0
- To disable or override the resource multiplier, set vespene geyser deaths above 0
- To check if a player slot is running the melee AI, check for at least 1 scanner sweep death

### Troubleshooting:
- Make sure your AI has the correct race set in map settings
  - For random AI, set the race to 'user select'
- For maps with optional AI support (i.e. vacant human slots converted to computers), make sure all forces have 'randomize start location' ticked off
  - This causes a critical issue with human -> computer conversions; a fix will be explored during 2021
- Results not guaranteed; the AI have only been verified with melee starting conditions
  - If you find a bug or run into trouble, feel free to reach out on discord!

# PERSONALITIES
- These use *disruption field* deaths.
- Their number corresponds to how many deaths you should set before running the AI.
- Without any deaths set, the personality will be chosen at random.

### TERRAN
- 1 - Mech
- 2 - Raider
- 3 - Air
- 4 - Shadow
- 5 - General
- 9 - Scenario (highly specific)

### PROTOSS
- 1 - Ranger
- 2 - Templar
- 3 - Skylord
- 4 - General
- 9 - Scenario (highly specific)

### ZERG
- 1 - Assault
- 2 - Swarm
- 3 - Air
- 4 - General
- 9 - Scenario (highly specific)

# BUILD ORDERS
- These use *dark swarm* deaths.
- Their number corresponds to how many deaths you should set before running the AI.
- Without any deaths set, the build order will be chosen at random.
	- Random build order selection includes safeguards to prevent more than 1 greed and 2 rush build order archetypes from being selected on the same team composition.
	- Obviously, it is trivial to override this safeguard via triggers if desired.

### TERRAN
#### Armor
- 1 - Timing - Phalanx (Factory first)
- 2 - Timing - FE 5Fact
- 3 - Rush - 2Fact Vult
- 4 - Rush - Walker Power
- 5 - Greed - Iron Crusade (3 base armor w/ Seraph Nuke)

#### Raider
- 1 - Timing - 8Rax
- 2 - Timing - 3Rax Marine & Cleric
- 3 - Greed - Fast Nuke
- 4 - Rush - 4Rax Pressure
- 5 - Timing - Uprising (heavy Cyprian usage)
- 6 - Rush - Holy Ackmeds!

#### Orbital
- 1 - Rush - 2Port Wraith
- 2 - Greed - The Don (3 base raider into Minotaur)
- 3 - Timing - Wraith Bio
- 4 - Timing - Hotdrop (bio + transport)

#### Shadow
- 1 - Rush - Royal Flush (future + mech)
- 2 - Timing - Unholy Trinity (biotic + infantry)
- 3 - Timing - Angels Above (tinkerer + air + infantry)
- 4 - Greed - The Verdict (fast Anticthon)

#### General
- 1 - Timing - 2Fact Mech
- 2 - Greed - Early Riser (12CC mix)
- 3 - Rush - The Cube (combine everything)
- 4 - Rush - Doubling Up (early 2 Barracks)
- 5 - Timing - Mech into Air
- 6 - Timing - Observant (Seraph + mech)

#### Scenario
- 1 - One for the Future - Wall Street War - Ullen "Bank On Me" Bigmunny

### PROTOSS
#### Ranger
- 1 - Timing - 2 Gate Strider
- 2 - Rush - 1 Gate Robo
- 3 - Timing - Forge FE Accantor
- 4 - Greed - 1Gate Expo
- 5 - Rush - Simulant Swarm

#### Templar
- 1 - Rush - Initiates (early Zealots)
- 2 - Timing - 3 Gate Strider
- 3 - Timing - 2 Gate Cabalist
- 4 - Timing - Legion
- 5 - Greed - Martyr (fast Grand Library)

#### Skylord
- 1 - Rush - FE Stargate
- 2 - Timing - Solar Escort
- 3 - Rush - 2 Gate Panoptus
- 4 - Greed - Exalted (fast Didact)
- 5 - Greed - Sorrow Fleet (fast Star Sovereign)

#### Profane
- 1 - Timing - The Banished (Stage-focused)
- 2 - Timing - Lost and Found (Gallery-focused)
- 3 - Rush - Cloak and Dagger (fast Cabalist/Barghest)
- 4 - Greed - Starbreach (fast Anthelion + fast 3-base)

#### General
- 1 - Greed - Robo FE
- 2 - Rush - Strident Striders
- 3 - Rush - 2 Gate Robo
- 4 - Timing - Lunatic Legion (Legionnaire + Aurora)

### ZERG
#### Assault
- 1 - Timing - 3Hatch Hydrok
- 2 - Rush - 2Hatch Zorkiz
- 3 - Rush - 2Hatch Hydrok-Quazrok
- 4 - Greed - 2Hatch Geszkath

#### Swarm
- 1 - Timing - 3Hatch Flood
- 2 - Timing - 3Hatch Ultrak
- 3 - Rush - Instant Quazrok
- 4 - Timing - 3Hatch Othstol
- 5 - Greed - Gates of Hell (fast 4 bases)

#### Sunblot
- 1 - Timing - 3Hatch Muthrok-Quazrok
- 2 - Greed - 2Hatch Geszithalor
- 3 - Rush - 2Hatch Mutalisk

#### Miscreant
- 1 - Timing - Symbiosis (Axitrilisk/Charlatan/Zorkiz/Quazrok)
- 2 - Timing - Former (Cohort/Swarming)
- 3 - Rush - Misbegotten Might (Cohort into Zorkiz)
- 4 - Greed - Rapid Annex (fast Empresses)

#### General
- 1 - Greed - Fast Othstol
- 2 - Timing - Swarming Zorkiz
- 3 - Timing - Hydrok Matravil
- 4 - Rush - Zorkiz Flood

# PREFERENCES
- Setting deaths for certain units will override default preferences.

### Notes:
- If competing preferences are set, one preference will be ignored.
- Preference deaths are converted to variables at script runtime, and must be configured prior to script execution to have any effect.
- Preferences do not guarantee compositions; for example, AI will default to lower-cost/tier units if it lacks income/tech.
- If no user-set preferences exist, the default preferences will sway between high tier and low tier depending on income.
- Setting deaths for a preference not listed here will (probably) not have any (useful) effect.

### TERRAN
#### Military
- Maverick < > Harakan < > Cyprian
- Madcap < > Heracles
- Cleric < > Shaman < > Apostle
- Eidolon < > Savant
- Olympian < > Autocrat
- Vulture < > Cyclops < > Goliath
- Southpaw < > Phalanx
- Madrigal < > Paladin
- Pazuzu < > Penumbra
- Wraith < > Gorgon < > Valkyrie
- Salamander < > Wyvern
- Seraph < > Azazel
- Centaur < > Minotaur < > Phobos
- Blackjack < > Cohort < > Aspirant

#### Production
- Barracks < > Captaincy
- Medbay < > Covert Ops
- Factory < > Iron Foundry
- Machine Shop < > Salvo Yard
- Starport < > Nanite Assembly
- Munitions Bay < > Control Tower

### PROTOSS
#### Military
- Zealot < > Legionnaire
- Dracadin < > Ecclesiast < > Hierophant
- Amaranth < > Atreus < > Archon
- Cantavis < > Augur
- Vassal < > Simulacrum < > Manifold
- Idol < > Golem
- Accantor < > Architect < > Demiurge
- Aurora < > Panoptus < > Lanifect
- Exemplar < > Gladius
- Magister < > Empyrean < > Didact
- Solarion < > Star Sovereign
- Cabalist < > Barghest
- Mind Tyrant < > Pariah
- Aspirant < > Charlatan < > Axitrilisk
- Striga < > Luminary < > Empress
- Clarion < > Monogram

#### Production
- Gateway < > Grand Library
- Robotics Facility < > Robotics Authority
- Stargate < > Argosy
- Prostration Stage < > Rogue Gallery

### ZERG
#### Military
- Zethrokor < > Vorvrokor 				[//]: # 'Swarm melee'
- Quazilisk < > Liiralisk 				[//]: # 'Swarm ranged'
- Hydralisk < > Bactalisk 				[//]: # 'Assault ranged'
- Skithrokor < > Gorgrokor				[//]: # 'Assault air'
- Mutalisk < > Vithrilisk 				[//]: # 'Air standard'
- Nathrokor < > Kalkalisk 				[//]: # 'Air swarming'
- Zoryusthaleth < > Ultrakor 			[//]: # 'Control melee'
- Lakizilisk 							[//]: # 'Control orphan'
- Isthrathaleth < > Sovroleth			[//]: # 'Control special'
- Matraleth < > Akistrokor				[//]: # 'Subjugate air'
- Konvilisk < > Almaksalisk				[//]: # 'Subjugate ranged'
- Keskathalor < > Alkajelisk			[//]: # 'Siege standard'
- Geszithalor							[//]: # 'Siege orphan'
- Cohort < > Axitrilisk					[//]: # 'Shared standard'
- Charlatan < > Tarasque < > Empress	[//]: # 'Shared advanced'
- Anticthon								[//]: # 'Shared ultimate'

#### Production
- Ovileth < > Iroleth < > Othstoleth < > Alaszileth
- Larval Colony < > Hatchery < > Lair < > Hive < > Sire

# ACKNOWLEDGEMENTS
- Modern AI work would not be possible without the contributions of Neiv, iquare, and Veeq7. Special thanks to them all!
- An additional special thanks goes out to Keyan and Connor5620, who have tested and documented several of the AI's build orders!